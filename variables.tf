
variable "vpc_cidr" {
    description = "Tells us the cidr block"
    type = string
    default = "11.0.0.0/16"

  
}

variable "vpc_tag" {
    description = "what tag"
    type = string
    default = "kojitech-vpc"
  
}

variable "pub_subnet_cidr" {
    type = string
    description = "public_sn cidr"
    default = "10.0.0.0/17"
  
}

variable "pri_sn_cidr" {
    type = string
    description = "private sn cidr"
    default = "10.0.128.0/17"
  
}

variable "sn_tags" {
    type = map
    default = {
        private_sn = "dev"
        public_sn = "prod"
    }
  
}


# types of variables
# "" = string
# bool = true/false
# number = 23
# list = [1,2,3,4] ["girl", "girl"]
# map = {
    #prod = "244553"
    #dev = "hhshhshs"
#}

