
# resource "aws_vpc" "second_day-practice" {
#   cidr_block = var.cidr_block

#   tags = {
#     env = var.vpc_tag.env
#   }
# }

# locals {
#   vpc_id = aws_vpc.second_day-practice.id
#   av_z   = var.az

# }

# resource "aws_instance" "ec2_with_Koji" {
#   ami           = "ami-09d3b3274b6c5d4aa"
#   instance_type = "t2.micro"
#   subnet_id = aws_subnet.pub_sn1.id

#   tags = {
#     Name = "HelloWorld"
#   }
# }

# output "Public_ip" {
#   value = aws_instance.ec2_with_Koji.public_ip
  
# }

# resource "aws_subnet" "pub_sn1" {
#   vpc_id                  = local.vpc_id
#   cidr_block              = "10.0.0.0/18"
#   map_public_ip_on_launch = true
#   availability_zone       = local.av_z

#   tags = {
#     env = var.sn_tag.env
#   }
# }

# resource "aws_subnet" "pub_sn2" {
#   vpc_id                  = local.vpc_id
#   cidr_block              = "10.0.64.0/18"
#   availability_zone       = local.av_z
#   map_public_ip_on_launch = true

#   tags = {
#     env = var.sn_tag.env
#   }
# }

# resource "aws_subnet" "pri_sn1" {
#   vpc_id            = local.vpc_id
#   cidr_block        = "10.0.128.0/18"
#   availability_zone = local.av_z

#   tags = {
#     env = var.sn_tag.Name
#   }
# }

# resource "aws_subnet" "pri_sn2" {
#   vpc_id            = local.vpc_id
#   cidr_block        = "10.0.192.0/18"
#   availability_zone = local.av_z

#   tags = {
#     env = var.sn_tag.Name
#   }
# }