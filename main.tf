
# Create a VPC
resource "aws_vpc" "kojitech-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "kojitech-vpc"
  }
}

resource "aws_vpc" "kojitech-vpc1" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = var.vpc_tag
  }
}

resource "aws_subnet" "public_sn" {
  vpc_id     = aws_vpc.kojitech-vpc.id
  cidr_block = var.pub_subnet_cidr
  availability_zone = "us-east-1a"

  tags = {
    Name = var.sn_tags.public_sn
  }
}

resource "aws_subnet" "private_sn" {
  vpc_id     = aws_vpc.kojitech-vpc.id
  cidr_block = var.pri_sn_cidr
  availability_zone = "us-east-1b"

  tags = {
    Name = var.sn_tags.private_sn
  }
}