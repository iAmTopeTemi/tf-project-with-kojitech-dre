
# resource "aws_vpc" "second_day-practice" {
#   cidr_block = var.cidr_block

#   tags = {
#     "env" = var.vpc_tag.env
#   }
# }

# #what if the logical name changes? means we have to replace vpc.id on each block. This is
# #redundant. so we create a locals block to make it easier and avoid repitition. see main-afterlocals.tf for best practice

# resource "aws_subnet" "pub_sn1" {
#   vpc_id                  = aws_vpc.second_day-practice.id
#   cidr_block              = "10.0.0.0/18"
#   map_public_ip_on_launch = true

#   tags = {
#     "env" = var.sn_tag.env
#   }
# }

# resource "aws_subnet" "pub_sn2" {
#   vpc_id                  = aws_vpc.second_day-practice.id
#   cidr_block              = "10.0.64.0/18"
#   availability_zone       = var.az
#   map_public_ip_on_launch = true

#   tags = {
#     "env" = var.sn_tag.env
#   }
# }

# resource "aws_subnet" "pri_sn1" {
#   vpc_id            = aws_vpc.second_day-practice.id
#   cidr_block        = "10.0.128.0/18"
#   availability_zone = var.az

#   tags = {
#     "env" = var.sn_tag.Name
#   }
# }

# resource "aws_subnet" "pri_sn2" {
#   vpc_id            = aws_vpc.second_day-practice.id
#   cidr_block        = "10.0.192.0/18"
#   availability_zone = var.az

#   tags = {
#     "env" = var.sn_tag.Name
#   }
# }