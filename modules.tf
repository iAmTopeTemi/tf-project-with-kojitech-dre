
data "aws_availability_zones" "still_trying" {
  state = "available"
}



module "vpc" {
  source = "terraform-aws-modules/vpc/aws"  #public-module-vpc

  name = "my-vpc"
  cidr = "10.0.0.0/16"

  azs             = slice(data.aws_availability_zones.still_trying.names, 0, 3)
  private_subnets = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
  public_subnets  = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

#>length(["eu-west-1a", "eu-west-1b", "eu-west-1c"])

#before slice 
# data.aws_availability_zones.still_trying.names will output 
# tolist([
#   "us-east-1a",
#   "us-east-1b",
#   "us-east-1c",
#   "us-east-1d",
#   "us-east-1e",
#   "us-east-1f",

# slice(data.aws_availability_zones.still_trying.names, 0, 3)
# tolist([
#   "us-east-1a",
#   "us-east-1b",
#   "us-east-1c",