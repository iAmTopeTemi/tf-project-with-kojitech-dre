
# locals {
#   vpc_id = aws_vpc.second_day-practice.id
#   av_z   = var.az

# }

# data "aws_availability_zones" "available" {
#   state = "available"
# }


# data "aws_key_pair" "key_pair_peering" {
#   key_name           = "keypair-for-peering"

# }

# #terraform apply -target data.aws_key_pair.key_pair_peering
# # you can use the above to pass a single block instruction. 
# # this might be useful in ds wso you can use tf console to check index





# data "aws_ami" "practice" {
#   most_recent      = true
#   owners           = ["amazon"]

#   filter {
#     name   = "name"
#     values = ["al2022-ami-minimal*"]
#   }

#   filter {
#     name   = "root-device-type"
#     values = ["ebs"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }
# }

# output "ami_id" {
#   value = data.aws_ami.practice.id
  
# }

# #to call data source on console paste data.aws_availability_zones.available.names(attribute) on console


# resource "aws_vpc" "second_day-practice" {
#   cidr_block = var.cidr_block

#   tags = {
#     env = var.vpc_tag.env
#   }
# }

# resource "aws_instance" "ec2_with_Koji" {
#   ami           = data.aws_ami.practice.id
#   instance_type = "t2.micro"
#   subnet_id = aws_subnet.pub_sn1.id
#   key_name = data.aws_key_pair.key_pair_peering.key_name

#   tags = {
#     Name = "HelloWorld"
#   }
# }

# resource "aws_subnet" "pub_sn1" {
#   vpc_id                  = local.vpc_id
#   cidr_block              = "10.0.0.0/18"
#   map_public_ip_on_launch = true
#   availability_zone       = data.aws_availability_zones.available.names[0]

#   tags = {
#     env = var.sn_tag.env
#   }
# }

# resource "aws_subnet" "pub_sn2" {
#   vpc_id                  = local.vpc_id
#   cidr_block              = "10.0.64.0/18"
#   availability_zone       = data.aws_availability_zones.available.names[0]
#   map_public_ip_on_launch = true

#   tags = {
#     env = var.sn_tag.env
#   }
# }

# resource "aws_subnet" "pri_sn1" {
#   vpc_id            = local.vpc_id
#   cidr_block        = "10.0.128.0/18"
#   availability_zone = data.aws_availability_zones.available.names[0]

#   tags = {
#     env = var.sn_tag.Name
#   }
# }

# resource "aws_subnet" "pri_sn2" {
#   vpc_id            = local.vpc_id
#   cidr_block        = "10.0.192.0/18"
#   availability_zone = data.aws_availability_zones.available.names[0]

#   tags = {
#     env = var.sn_tag.Name
#   }
# }

# output "Public_ip" {
#   value = aws_instance.ec2_with_Koji.public_ip
  
# }
